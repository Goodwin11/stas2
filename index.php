<?php
$age = $_POST['age'];
$data = [
    [ 
        'name' => 'Grayson Allen' , 
        'group' => 'administrator', 
        'email' => 'john@gmail.com', 
        'phone' =>'123-675-34'
    ],
    [ 
        'name' => 'Steven Adams' , 
        'group' => 'student', 
        'email' => 'gray23@gmail.com', 
        'phone' =>'123-346-65'
    ],
    [ 
        'name' => 'Kostas Antetokoumpo' , 
        'group' => 'teacher', 
        'email' => 'king-gary@gmail.com', 
        'phone' =>'123-475-56'
    ],
    [ 
        'name' => 'Dwayne Bacon' , 
        'group' => 'administrator', 
        'email' => 'StreetGarden@gmail.com', 
        'phone' =>'123-787-67'
    ],
    [ 
        'name' => 'No Bamba' , 
        'group' => 'student', 
        'email' => 'IceMan@gmail.com', 
        'phone' =>'123-345-00'
    ],
    [ 
        'name' => 'Lu Wiliams' , 
        'group' => 'student', 
        'email' => 'Ololosh@gmail.com', 
        'phone' =>'123-346-56'
    ],
    [ 
        'name' => 'Wil Barton' , 
        'group' => 'teacher', 
        'email' => 'BorisBritwa@gmail.com', 
        'phone' =>'123-656-55'
    ],
    [ 
        'name' => 'Bradley Beal' , 
        'group' => 'administrator', 
        'email' => 'GoldPen@gmail.com', 
        'phone' =>'123-324-44'
    ],
    [ 
        'name' => 'Khem Birch' , 
        'group' => 'student', 
        'email' => 'WhiteBeerInBadDay@gmail.com', 
        'phone' =>'123-344-50'
    ],
    [ 
        'name' => 'Eric Bledsoe' , 
        'group' => 'teacher', 
        'email' => 'ShootMoreAmmo@gmail.com', 
        'phone' =>'123-343-42'
    ],
]

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
	
</head>
<body>
<form action="/" method="POST" class="form-inline">
<div class="container">
    <div>
        <label for="">Please enter a name</label>
        <input type="text" name="name" >
    </div>
    <p><select size="1" name="group" >
        <option disabled>Select a position</option>
        <option value="Student">Student</option>
        <option value="Teacher">Teacher</option>
        <option value="administrator">administrator</option>
    </select></p>
    <div>
        <label for="">Please enter your age</label>
        <input type="text" name="age">
        <?php if($_POST['age'] > 17):?>
        <?php else:?>
        <h3>You are to young!!!</h3>
        <?php endif;?>
    </div>
    <div>
        <label for="">Please enter email</label>
        <input type="text" name="email">
    </div>
    <div>
        <label for="">Please enter phone number</label>
        <input type="text" name="phone">
    </div>
    <button>Refresh</button>
</form>
</div>
<div class="container">
     <h3>Congratulations, you have successfully added to the list <?=$_POST['group']?></h3>
<table class="table">
    <tr>
        <th>Name</th>
        <th>Group</th>
        <th>Email</th>
        <th>Number</th>
    </tr>
        <?php foreach ($data as $key => $crew):?>
    <tr>
        <td><?=$crew['name']?></td>
        <td><?=$crew['group']?></td>
        <td><?=$crew['email']?></td>
        <td><?=$crew['phone']?></td>
    </tr>
    <tr>
         <?php endforeach;?>
            <tr>
                <td><?=$_POST['name']?></td>
                <td><?=$_POST['group']?></td>
                <td><?=$_POST['email']?></td>
                <td><?=$_POST['phone']?></td>
            </tr>
</table>
</div>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>