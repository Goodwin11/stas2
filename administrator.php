<?php
$data = [
    [ 
        'name' => 'Grayson Allen' , 
        'group' => 'administrator', 
        'email' => 'john@gmail.com', 
        'phone' =>'123-675-34'
    ],
    [ 
        'name' => 'Steven Adams' , 
        'group' => 'student', 
        'email' => 'gray23@gmail.com', 
        'phone' =>'123-346-65'
    ],
    [ 
        'name' => 'Kostas Antetokoumpo' , 
        'group' => 'teacher', 
        'email' => 'king-gary@gmail.com', 
        'phone' =>'123-475-56'
    ],
    [ 
        'name' => 'Dwayne Bacon' , 
        'group' => 'administrator', 
        'email' => 'StreetGarden@gmail.com', 
        'phone' =>'123-787-67'
    ],
    [ 
        'name' => 'No Bamba' , 
        'group' => 'student', 
        'email' => 'IceMan@gmail.com', 
        'phone' =>'123-345-00'
    ],
    [ 
        'name' => 'Lu Wiliams' , 
        'group' => 'student', 
        'email' => 'Ololosh@gmail.com', 
        'phone' =>'123-346-56'
    ],
    [ 
        'name' => 'Wil Barton' , 
        'group' => 'teacher', 
        'email' => 'BorisBritwa@gmail.com', 
        'phone' =>'123-656-55'
    ],
    [ 
        'name' => 'Bradley Beal' , 
        'group' => 'administrator', 
        'email' => 'GoldPen@gmail.com', 
        'phone' =>'123-324-44'
    ],
    [ 
        'name' => 'Khem Birch' , 
        'group' => 'student', 
        'email' => 'WhiteBeerInBadDay@gmail.com', 
        'phone' =>'123-344-50'
    ],
    [ 
        'name' => 'Eric Bledsoe' , 
        'group' => 'teacher', 
        'email' => 'ShootMoreAmmo@gmail.com', 
        'phone' =>'123-343-42'
    ],
]
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- CSS only -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
<div class="container">
<table class="table">
    <tr>
        <th>Name</th>
        <th>Group</th>
        <th>Email</th>
        <th>Number</th>
    </tr>
        <?php foreach ($data as $key => $crew):?>
    <tr> <?php if($crew['group'] === 'administrator'):?>
        <td><?=$crew['name']?></td>
        <td><?=$crew['group']?></td>
        <td><?=$crew['email']?></td>
        <td><?=$crew['phone']?></td>
    </tr>
    <?php endif;?>
    <?php endforeach;?>
    </table>
</div>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>